import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form } from "react-bootstrap";

export default class ItemForm extends Component {
  render() {
    return (
      <div>
        <Form.Group>
          <Form.Label>Title</Form.Label>
          <Form.Control type="text" placeholder="Title" onChange={(e)=>this.props.onChangeTitle(e)} />
        </Form.Group>
        <Form.Group>
          <Form.Label>Message</Form.Label>
         <textarea className="form-control" placeholder="Message" onChange={(e)=>this.props.onChangeMessage(e)}/>
        </Form.Group>
      </div>
    );
  }
}
