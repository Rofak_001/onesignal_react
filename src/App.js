import OneSignal from "react-onesignal";
import React, { Component } from "react";
import Axios from "axios";
import ItemForm from "./ItemForm";
import { Container } from "react-bootstrap/esm";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      message: "",
      userId:""
    };
  }
  componentWillMount() {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then((response) => response.json())
      .then((json) => {
        console.log(json.userId)
        this.setState({userId:json.userId})
      });
    const events = [
      {
        listener: "once",
        event: "subscriptionChange",
        callback: (isSubscribed) => {
          if (true === isSubscribed) {
            console.log("The user subscription state is now:", isSubscribed);
          }
        },
      },
      {
        event: "notificationDisplay",
        callback: (event) => {
          console.warn("OneSignal notification displayed:", event);
        },
      },
      {
        event: "notificationDismiss",
        callback: (event) => {
          console.warn("OneSignal notification dismissed:", event);
        },
      },
    ];
    OneSignal.initialize(
      "ef82e6c2-451f-4e58-bb32-3ea2b5492b7b",
      {
        autoRegister: true,
        autoResubscribe: true,
        allowLocalhostAsSecureOrigin: true,
        notifyButton: {
          enable: true,
          size: "medium",
          position: "bottom-right",
        },
      },
      events
    );
  }
  send = () => {
    var sendMsg = {
      app_id: "ef82e6c2-451f-4e58-bb32-3ea2b5492b7b",
      contents: {
        en: this.state.message,
      },
      headings: {
        en: this.state.title,
      },
      url: "https://onesignal.com",
      "include_external_user_ids":[this.state.userId]
    };
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Basic Mzk0NjJhYjYtMzRhOS00MjM1LTg4OWEtOGRkODJkZTZjYTY3",
    };
    Axios.post("https://onesignal.com/api/v1/notifications", sendMsg, {
      headers: headers,
    }).then((res) => {
      console.log(res);
    });
  };
  async get() {
    const playerId = await OneSignal.getPlayerId();
    console.log(playerId);
    console.log("userID:"+this.state.userId)
    OneSignal.setExternalUserId(this.state.userId)
    OneSignal.registerForPushNotifications()
  }
  onChangeTitle = (e) => {
    this.setState({ title: e.target.value });
    // console.log(e.target.value)
  };
  onChangeMessage = (e) => {
    this.setState({ message: e.target.value });
    // console.log(e.target.value)
  };
  render() {
  
    return (
      <Container>
        <ItemForm
          onChangeMessage={this.onChangeMessage}
          onChangeTitle={this.onChangeTitle}
        />
        <div className="text-center">
          <button
            className="btn btn-primary mx-2"
            onClick={this.send.bind(this)}
          >
            Send
          </button>
          <button className="btn btn-success" onClick={this.get.bind(this)}>
            getUserId
          </button>
        </div>
      </Container>
    );
  }
}
